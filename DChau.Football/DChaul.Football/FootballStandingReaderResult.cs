﻿using System.Collections.Generic;
using DChau.Football.Model;

namespace DChau.Football
{
    /// <summary>
    /// Specifies details of the read result.
    /// </summary>
    public class FootballStandingReaderResult
    {
        /// <summary>
        /// Initialize a new instance of FootballStandingReaderResult
        /// </summary>
        internal FootballStandingReaderResult()
        {
            Errors = new List<FootballStandingReadError>();
        }

        /// <summary>
        /// The resulting league constructed.
        /// </summary>
        public FootballLeague Result
        {
            get;
            internal set;
        }

        /// <summary>
        /// List of errors during reading of the data.
        /// </summary>
        public IList<FootballStandingReadError> Errors
        {
            get; 
            private set; 
        }
    }
}
