﻿namespace DChau.Football
{
    /// <summary>
    /// Specifies details of an error during the read of football standing data
    /// </summary>
    public class FootballStandingReadError
    {
        /// <summary>
        /// Line number of the error
        /// </summary>
        public int LineNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Error message of reason the read failed for line
        /// </summary>
        public string ErrorMessage
        {
            get;
            set;
        }
    }
}
