﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DChau.Football.Model
{
    /// <summary>
    /// Specifies details of a football league. 
    /// </summary>
    public class FootballLeague
    {
        private readonly IList<FootballTeam> teams; 

        /// <summary>
        /// Initialize a new instance of FootballLeague
        /// </summary>
        public FootballLeague()
        {
            Id = Guid.NewGuid();
            teams = new List<FootballTeam>(20);
        }

        /// <summary>
        /// Unique identifier for the league.
        /// </summary>
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Name of the league.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Collection of teams in the league.
        /// </summary>
        public IEnumerable<FootballTeam> Teams
        {
            get
            {
                return teams;
            }
        }

        /// <summary>
        /// Returns the team with the mimimum difference in goals for and goals against.
        /// </summary>
        /// <returns></returns>
        public FootballTeam TeamWithMinDifferenceInForAndAgainstGoals()
        {
            return teams.OrderBy(t => t.DifferenceInForAndAgainstGoals).FirstOrDefault();
        }

        /// <summary>
        /// Add a team to the league.
        /// </summary>
        /// <param name="team"></param>
        public void AddTeam(FootballTeam team)
        {
            if (!teams.Contains(team))
                teams.Add(team);
        }
    }
}
