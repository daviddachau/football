﻿using System;

namespace DChau.Football.Model
{
    /// <summary>
    /// Specifies details of a football team in a league
    /// and its statistics in the competition.
    /// </summary>
    public class FootballTeam
    {
        /// <summary>
        /// Initialize a new instance of FootballTeam
        /// </summary>
        public FootballTeam()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Unique identifier for the team
        /// </summary>
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Name of the team
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Number of games played
        /// </summary>
        public int GamesPlayed
        {
            get;
            set;
        }

        /// <summary>
        /// Number of games won
        /// </summary>
        public int Wins
        {
            get;
            set;
        }

        /// <summary>
        /// Number of games lost
        /// </summary>
        public int Losses
        {
            get;
            set;
        }

        /// <summary>
        /// Number of games drawn
        /// </summary>
        public int Draws
        {
            get;
            set;
        }

        /// <summary>
        /// Goals kicked against other opponents
        /// </summary>
        public int GoalsFor
        {
            get;
            set;
        }

        /// <summary>
        /// Goals opponents kicked against the team
        /// </summary>
        public int GoalsAgainst
        {
            get;
            set;
        }

        /// <summary>
        /// Total points earned in the competition which determines the standings
        /// </summary>
        public int Points
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the difference in goals for and against the team
        /// </summary>
        public int DifferenceInForAndAgainstGoals
        {
            get
            {
                return Math.Abs(GoalsFor - GoalsAgainst);
            }
        }

    }
}
