﻿using System;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using DChau.Football.Model;

namespace DChau.Football.Csv
{
    /// <summary>
    /// Reads data from CSV containing football standing information
    /// </summary>
    public class CsvFootballStandingReader : IFootballStandingReader
    {
        private readonly Stream stream;

        /// <summary>
        /// Initialize a new instance of CsvFootballStandingReader with the stream 
        /// containing the CSV data.
        /// </summary>
        public CsvFootballStandingReader(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            this.stream = stream;
        }

        /// <summary>
        /// Reads the data and constructs a <see cref="FootballLeague"/>.
        /// </summary>
        /// <returns></returns>
        public FootballStandingReaderResult Read()
        {
            var readerResult = new FootballStandingReaderResult();

            var footBallLeague = new FootballLeague();

            using (var streamReader = new StreamReader(stream))
            {
                var csvReader = new CsvReader(streamReader);

                csvReader.Configuration.RegisterClassMap<FootballCsvMap>();
                csvReader.Configuration.HasHeaderRecord = true;
                csvReader.Configuration.IgnoreReadingExceptions = true;
                csvReader.Configuration.ReadingExceptionCallback +=
                    (exception, reader) => readerResult.Errors.Add(new FootballStandingReadError
                        {
                            LineNumber = reader.Row,
                            ErrorMessage = exception.Message
                        });

                // TODO: validate field headers against expected so we do not do unneccesary parsing of the file
                // TODO: when not in the expected format.

                var records = csvReader.GetRecords<FootballTeam>();

                foreach (var footballTeam in records)
                    footBallLeague.AddTeam(footballTeam);
            }

            readerResult.Result = footBallLeague;

            return readerResult;
        }

        /// <summary>
        /// Maps the Football class properties to the CSV fields.
        /// </summary>
        private class FootballCsvMap : CsvClassMap<FootballTeam>
        {
            public override void CreateMap()
            {
                Map(m => m.Name).ConvertUsing(row =>
                {
                    var teamName = row.GetField<string>("Team");

                    //TODO: Strip the ranking number off the team name

                    return teamName;
                });


                // TODO: Extract CSV definition to another class instead of hardcoding column names here

                Map(m => m.GamesPlayed).Name("P");
                Map(m => m.Wins).Name("W");
                Map(m => m.Losses).Name("L");
                Map(m => m.Draws).Name("D");
                Map(m => m.GoalsFor).Name("F");
                Map(m => m.GoalsAgainst).Name("A");
                Map(m => m.Points).Name("Pts");
            }
        }
    }
}
