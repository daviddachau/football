﻿using DChau.Football.Model;

namespace DChau.Football
{
    /// <summary>
    /// Defines common methods to read football standing data.
    /// </summary>
    public interface IFootballStandingReader
    {
        /// <summary>
        /// Reads the data and constructs a constructs a <see cref="FootballLeague"/>.
        /// </summary>
        /// <returns></returns>
        FootballStandingReaderResult Read();
    }
}
