﻿using DChau.Football.Model;
using NUnit.Framework;

namespace DChau.Football.Tests.Model
{
    [TestFixture]
    public class FootballTeamTests
    {
        [Test]
        public void DifferenceInForAndAgainstGoals_FiveForTwoAgainst_ThreeDifference()
        {
            // Arrange

            var team = new FootballTeam
                {
                    GoalsFor = 5,
                    GoalsAgainst = 2
                };

            // Act

            var difference = team.DifferenceInForAndAgainstGoals;

            // Assert

            Assert.AreEqual(3, difference);
        }

        [Test]
        public void DifferenceInForAndAgainstGoals_TwoForFiveAgainst_ThreeDifference()
        {
            // Arrange

            var team = new FootballTeam
            {
                GoalsFor = 5,
                GoalsAgainst = 2
            };

            // Act

            var difference = team.DifferenceInForAndAgainstGoals;

            // Assert

            Assert.AreEqual(3, difference, "Expect result to be absolute value as it is a difference calc.");
        }
    }
}
