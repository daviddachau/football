﻿using DChau.Football.Model;
using NUnit.Framework;
using System.Linq;

namespace DChau.Football.Tests.Model
{
    [TestFixture]
    public class FootballLeagueTests
    {
        [Test]
        public void TeamWithMinDifferenceInForAndAgainstGoals_ArsenalTeamHasMinDiff_ArsenalReturned()
        {
            // Arrange

            var league = new FootballLeague();

            var arsenal = new FootballTeam
                {
                    Name = "Arsenal",
                    GoalsFor = 1,
                    GoalsAgainst = 1
                };

            var chelsea = new FootballTeam
                {
                    Name = "Chelsea",
                    GoalsFor = 23,
                    GoalsAgainst = 1
                };

            league.AddTeam(arsenal);
            league.AddTeam(chelsea);

            // Act

            var result = league.TeamWithMinDifferenceInForAndAgainstGoals();

            // Assert

            Assert.AreEqual(arsenal, result, "Expected Arsenal as a difference for the team is 0. Chelsea is 22.");
        }

        [Test]
        public void TeamWithMinDifferenceInForAndAgainstGoals_TwoTeamsSameDiff_FirstInListReturned()
        {
            // Arrange

            var league = new FootballLeague();

            var arsenal = new FootballTeam
            {
                Name = "Arsenal",
                GoalsFor = 23,
                GoalsAgainst = 1
            };

            var chelsea = new FootballTeam
            {
                Name = "Chelsea",
                GoalsFor = 23,
                GoalsAgainst = 1
            };

            league.AddTeam(chelsea);
            league.AddTeam(arsenal);

            // Act

            var result = league.TeamWithMinDifferenceInForAndAgainstGoals();

            // Assert

            Assert.AreEqual(chelsea, result, "Expected Chelsea - both teams have same difference and chelsea added to team list first");
        }

        [Test]
        public void AddTeam_NewTeam_TeamAddedToLeague()
        {
            // Arrange

            var league = new FootballLeague();

            var arsenal = new FootballTeam
            {
                Name = "Arsenal",
                GoalsFor = 23,
                GoalsAgainst = 1
            };

            // Act
            
            league.AddTeam(arsenal);

            // Assert

            Assert.IsNotNull(league.Teams.FirstOrDefault(p => p == arsenal), "Expect to have arsenal in list of teams");
        }

        [Test]
        public void AddTeam_ExistingTeam_TeamNotAddedAgainToLeague()
        {
            // Arrange

            var league = new FootballLeague();

            var arsenal = new FootballTeam
            {
                Name = "Arsenal",
                GoalsFor = 23,
                GoalsAgainst = 1
            };

            league.AddTeam(arsenal);

            // Act

            league.AddTeam(arsenal);

            // Assert

            Assert.IsNotNull(league.Teams.SingleOrDefault(p => p == arsenal), "Expect to have only one arsenal in list of teams");
        }
    }
}
