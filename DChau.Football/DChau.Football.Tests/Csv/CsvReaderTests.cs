﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using DChau.Football.Csv;
using NUnit.Framework;

namespace DChau.Football.Tests.Csv
{
    [TestFixture]
    public class CsvReaderTests
    {
        /// <summary>
        /// This test runs the provided CSV and outputs the team with the minimum difference in goals for and against. 
        /// </summary>
        [Test]
        public void Excercise()
        {
            var csvStream = LoadXmlResource("TestData.football.csv");
            var csvReader = new CsvFootballStandingReader(csvStream);

            var results = csvReader.Read();

            var team = results.Result.TeamWithMinDifferenceInForAndAgainstGoals();

            Debug.WriteLine(string.Format("Team with the smallest difference in 'for' and 'against' goals: {0}",
                                          team.Name));
        }

        [Test]
        public void Read_CsvWithOneTeam_LeagueCreatedWithTeamDataPopulated()
        {
            // Arrange

            var csvStream = LoadXmlResource("TestData.football-oneteam.csv");
            var csvReader = new CsvFootballStandingReader(csvStream);

            // Act

            var results = csvReader.Read();

            // Assert

            Assert.IsNotNull(results.Result, "Expected a football league to be created.");
            Assert.AreEqual(1, results.Result.Teams.Count(), "Expected 1 teams to be created to reflect the number of teams in the csv.");

            var team = results.Result.Teams.First();

            Assert.AreEqual("Arsenal", team.Name);
            Assert.AreEqual(38, team.GamesPlayed);
            Assert.AreEqual(26, team.Wins);
            Assert.AreEqual(9, team.Losses);
            Assert.AreEqual(3, team.Draws);
            Assert.AreEqual(79, team.GoalsFor);
            Assert.AreEqual(36, team.GoalsAgainst);
            Assert.AreEqual(87, team.Points);
        }

        [Test]
        public void Read_CsvWithMissingColumn_NoTeamCreatedAndOneErrorReported()
        {
            // Arrange

            var csvStream = LoadXmlResource("TestData.football-oneteam-missingcolumn.csv");
            var csvReader = new CsvFootballStandingReader(csvStream);

            // Act

            var results = csvReader.Read();

            // Assert

            Assert.IsNotNull(results.Result, "Expected a football league to be created.");
            Assert.AreEqual(0, results.Result.Teams.Count(), "Expected the team to not be created as there was a missing column.");
            Assert.AreEqual(1, results.Errors.Count);

            var error = results.Errors.First();

            Assert.AreEqual(2, error.LineNumber, "Missing column was on line 2");
            Assert.IsNotNullOrEmpty(error.ErrorMessage, "Expect an error message with reason why the parse was not successful");
        }

        [Test]
        public void Read_DatFile_NoTeamsCreatedAndManyErrorsReportedForEachRow()
        {
            // Arrange

            var csvStream = LoadXmlResource("TestData.football.dat");
            var csvReader = new CsvFootballStandingReader(csvStream);

            // Act

            var results = csvReader.Read();


            // Assert

            Assert.IsNotNull(results.Result, "Expected a football league to be created.");
            Assert.AreEqual(0, results.Result.Teams.Count(), "Expected no teams as this is an invalid file.");
            Assert.AreNotEqual(0, results.Errors.Count, "Expect many errors in trying to parse each line.");
        }

        private Stream LoadXmlResource(string resourceName)
        {
            var expectedResultStream =
                GetType().Assembly.GetManifestResourceStream(GetType().Namespace + "." + resourceName);

            return expectedResultStream;
        }
    }
}